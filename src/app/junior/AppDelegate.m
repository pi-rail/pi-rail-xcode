//
//  AppDelegate.m
//  PI-App-Template
//
//  Created by pi-data on 11.05.18.
//  Copyright © 2018 PI-Data. All rights reserved.
//

#import <JRE.h>
#import "AppDelegate.h"
#import "MainViewController.h"
#import "UIFactoryRailIOS.h"

#include "de/pidata/qnames/QName.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/service/base/ParameterType.h"
#include "de/pidata/service/base/ParameterList.h"
#include "de/pidata/service/base/AbstractParameterList.h"
#include "de/pidata/models/tree/Context.h"

#include "de/pidata/gui/component/base/Platform.h"
#include "de/pidata/gui/guidef/ControllerBuilder.h"
#include "de/pidata/gui/guidef/DialogDef.h"

#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/AbstractDialogController.h"

#include "de/pidata/system/ios/IOSSystem.h"
#include "de/pidata/gui/ios/platform/IOSPlatform.h"
#include "de/pidata/gui/ios/controller/IOSDialog.h"

#include "J2ObjC_source.h"
#include "de/pidata/system/base/Storage.h"
#include "de/pidata/string/Helper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

UIWindow * window;
PIIY_IOSSystem * systemManager;
PIIP_IOSPlatform * platform;
PIMR_Context * context;
UIAlertView *alert;
bool piMobileInitialized = false;
MainViewController *mainViewController;
NSString * appVersionString;
NSString* currentAppLanguage;
NSString * programName;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
  // Ignore SIGPIPE: It is sent by socket errors and will stop active thread instead of rising an Exception
  signal(SIGPIPE, SIG_IGN);
  
  // Disable the autolayout errors
  [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"_UIConstraintBasedLayoutLogUnsatisfiable"];
  
  JavaxNetSslHttpsURLConnection_class_();
  
  // Override point for customization after application launch.
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  self.window.backgroundColor = [UIColor systemBackgroundColor];
  
  alert = [[UIAlertView alloc] initWithTitle:@"Loading..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
  UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
  indicator.center = alert.center;
  [alert setValue:indicator forKey:@"accessoryView"];
  [indicator startAnimating];
  [alert show];
  
  // avoid display dimming and off
  [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
  
  mainViewController = [[MainViewController alloc] initWithNibName:@"mobile_main" bundle:[NSBundle mainBundle]];
  mainViewController.modalPresentationStyle = UIModalPresentationFullScreen;
  
  /**
   Add Menu an BarButton items
   */
  
  UIImage *ellipsisImage = [[UIImage systemImageNamed:@"ellipsis.circle"] imageWithRenderingMode:UIImageRenderingModeAutomatic];
  UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:ellipsisImage style:UIBarButtonItemStylePlain target:self action:nil];
  mainViewController.navigationItem.rightBarButtonItem = menuButton;
  
  UIImage *logo = [[UIImage imageNamed:@"ctc_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
  UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
  button.imageView.layer.cornerRadius = 8;
  [button setImage:logo forState:UIControlStateNormal];
  UIBarButtonItem *logoButton = [[UIBarButtonItem alloc] initWithCustomView:button];
  
  mainViewController.navigationItem.leftBarButtonItem = logoButton;
  
  
  programName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
  appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
  NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
  NSString * programVersion = [NSString stringWithFormat:@"%@ (%@)", appVersionString, appBuildString];
  
  // Create a navigationcontroller and init with our viewcontroller -> later we can add childdialogs pushin them on the
  // navigationcontroller -> this provides native backbutton and navigation through the hierarchie.
  UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
  navController.modalPresentationStyle = UIModalPresentationFullScreen;
  
  // Add the navigationcontroller as the rootViewController!
  self.window.rootViewController = navController;
  [self.window makeKeyAndVisible];
  
  systemManager = [[DePidataSystemIosIOSSystem alloc] initWithNSString:@"." withNSString:programVersion withNSString:programName];
  context = [[PIMR_Context alloc] initWithNSString:nil withPIQ_QName:nil withNSString:programName withNSString:programVersion];
  id factory = [[DePidataRailIosUIFactoryRailIOS alloc] init];
  platform = [[DePidataGuiIosPlatformIOSPlatform alloc] initWithPIMR_Context:context];
  [platform setStartupProgressWithInt:100];
  
  /**
   Create the Menu after the Systemmanager exists.
   */
  currentAppLanguage = [[[[NSLocale preferredLanguages] firstObject] componentsSeparatedByString:@"-"] objectAtIndex:0];
  NSMutableArray * mainMenu = [self createMenu: currentAppLanguage];
  UIMenu* menu = [UIMenu menuWithTitle:@"" children:mainMenu];
  menuButton.menu = menu;
  
  return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  if (!piMobileInitialized) {
    // [self performSelector:@selector(initialize) withObject:nil afterDelay:0.5f];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      [self initialize];
    }];
  }
}

- (void)initialize {
  PIGG_ControllerBuilder * builder = [[PIGB_Platform getInstance] getControllerBuilder];
  PIQ_Namespace * ns = [PIQ_Namespace getInstanceWithNSString:@"de.pidata.gui"];
  PIQ_QName * dialogID = [ns getQNameWithNSString:@"mobile_main"];
  PIGG_DialogDef * dialogDef = [builder getDialogDefWithPIQ_QName:dialogID];
  if (dialogDef != nil) {
    PIIC_IOSDialog * iosDialog = [PIIC_IOSDialog createDialogWithPIQ_QName:dialogID withPIGG_DialogDef:dialogDef withPIIC_UIViewControllerPI:mainViewController];
    id<PIGC_DialogController> dlgCtrl = [builder createDialogControllerWithPIQ_QName:dialogID withPIMR_Context:context withPIGE_Dialog:iosDialog withPIGC_DialogController:nil];
    
      [dlgCtrl setTitleWithNSString:[NSString stringWithFormat:@"%@ %@", programName, appVersionString]];
    
    id<PISB_ParameterList> paramList = [[PISB_AbstractParameterList alloc] initWithInt:0];
    [dlgCtrl setParameterListWithPISB_ParameterList:paramList];
    [dlgCtrl activateWithPIGU_UIContainer:iosDialog];
    piMobileInitialized = true;
    
    [alert dismissWithClickedButtonIndex:0 animated:YES];
  }
  
}


- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSMutableArray *)createMenu:(NSString *) language {
  
  /**
   For the menu we need to get the localized sttings right here, because the titles of UIMenu items can not be changed after they are created.
   */

  JavaUtilProperties *langProps = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLanguagePropsWithNSString:@"mobile_main" withNSString:language];
  JavaUtilProperties *glossaryProps = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getGlossaryPropsWithNSString:language];
  
  
  
  NSString * currentItemText;
  NSString * itemIdentifier;
    
  /**
   Help
   */
  NSMutableArray* helpActions = [[NSMutableArray alloc] init];
  
  itemIdentifier = @"sendlog_btn";
  currentItemText = PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_([langProps getPropertyWithNSString:itemIdentifier], @"{", @"}", glossaryProps);
  [helpActions addObject:[UIAction actionWithTitle:currentItemText image:nil identifier:itemIdentifier handler:^(__kindof UIAction* _Nonnull action) {
    [[mainViewController getIosDialog] menuSelectedWithNSString:itemIdentifier];
  }]];
  
  
  itemIdentifier = @"help_menu";
  currentItemText = PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_([langProps getPropertyWithNSString:itemIdentifier], @"{", @"}", glossaryProps);
  UIMenu* helpMenu = [UIMenu menuWithTitle:currentItemText image:[UIImage systemImageNamed:@"ellipsis.circle"] identifier:itemIdentifier options:0 children:helpActions];
  
  
  /**
   Language
   */
  NSMutableArray* languageActions = [[NSMutableArray alloc] init];
  
  itemIdentifier = @"deutsch_btn";
  currentItemText = PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_([langProps getPropertyWithNSString:itemIdentifier], @"{", @"}", glossaryProps);
  [languageActions addObject:[UIAction actionWithTitle:currentItemText image:nil identifier:itemIdentifier handler:^(__kindof UIAction* _Nonnull action) {
    [[mainViewController getIosDialog] menuSelectedWithNSString:itemIdentifier];
  }]];
  
  itemIdentifier = @"english_btn";
  currentItemText = PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_([langProps getPropertyWithNSString:itemIdentifier], @"{", @"}", glossaryProps);
  [languageActions addObject:[UIAction actionWithTitle:currentItemText image:nil identifier:itemIdentifier handler:^(__kindof UIAction* _Nonnull action) {
    [[mainViewController getIosDialog] menuSelectedWithNSString:itemIdentifier];
  }]];
  
  itemIdentifier = @"french_btn";
  currentItemText = PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_([langProps getPropertyWithNSString:itemIdentifier], @"{", @"}", glossaryProps);
  [languageActions addObject:[UIAction actionWithTitle:currentItemText image:nil identifier:itemIdentifier handler:^(__kindof UIAction* _Nonnull action) {
    [[mainViewController getIosDialog] menuSelectedWithNSString:itemIdentifier];
  }]];
  
  
  itemIdentifier = @"menu_Language";
  currentItemText = PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_([langProps getPropertyWithNSString:itemIdentifier], @"{", @"}", glossaryProps);
  UIMenu* languageMenu = [UIMenu menuWithTitle:currentItemText image:[UIImage systemImageNamed:@"ellipsis.circle"] identifier:itemIdentifier options:0 children:languageActions];
  
  /**
   Create main menu
   */
  
  NSMutableArray* mainMenu = [[NSMutableArray alloc] init];
  
  itemIdentifier = @"configurator_btn";
  currentItemText = PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_([langProps getPropertyWithNSString:itemIdentifier], @"{", @"}", glossaryProps);
  [mainMenu addObject:[UIAction actionWithTitle:currentItemText image:nil identifier:itemIdentifier handler:^(__kindof UIAction* _Nonnull action) {
    [[mainViewController getIosDialog] menuSelectedWithNSString:itemIdentifier];
  }]];
  
  
  [mainMenu addObject:helpMenu];
  [mainMenu addObject:languageMenu];
  
  return mainMenu;
}

- (void) changeMenuLanguage: (NSString*) language {
  if (![currentAppLanguage isEqualToString:language]) {
    currentAppLanguage = language;
    NSMutableArray * mainMenu = [self createMenu: language];
    UIMenu* menu = [UIMenu menuWithTitle:@"" children:mainMenu];
    
    mainViewController.navigationItem.rightBarButtonItem.menu = menu;
  }
}


@end
