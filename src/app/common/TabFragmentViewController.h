#import <UIKit/UIKit.h>
#import "DialogControllerDelegate.h"
#import "UIViewControllerPI.h"

@interface TabFragmentViewController : PIIC_UIViewControllerPI

- (void)showSelectedTab:(NSInteger)tabIndex;

@end
