#import "CardView.h"

static CGFloat radius = 8;


@implementation CardView


-(void)layoutSubviews{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = radius > 0;
    
    self.layer.borderWidth = 0.8f;
    self.layer.borderColor = [UIColor secondarySystemFillColor].CGColor;

}

-(id)initWithCoder:(NSCoder *)aDecoder{
    return [super initWithCoder:aDecoder];
}

@end
