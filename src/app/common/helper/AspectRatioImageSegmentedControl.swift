//
//  AspectRationImageSegmentedControl.swift
//  PI-Rail-XCode
//
//  Created by cga on 10.05.21.
//  Copyright © 2021 PI-Data AG. All rights reserved.
//

import Foundation
import UIKit

class AspectRationImageSegmentedControl : UISegmentedControl {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setAspectRatioForImages()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setAspectRatioForImages()
    }
    
    func setAspectRatioForImages() {
        self.subviews.flatMap{$0.subviews}.forEach { subview in
            if let imageView = subview as? UIImageView, let image = imageView.image, image.size.width > 5 {
                // The imageView which isn't separator
                imageView.contentMode = .scaleAspectFit
                imageView.image = imageView.image?.withRenderingMode(.alwaysOriginal)
            }
        }
    }
}
