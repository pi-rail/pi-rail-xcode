//
//  PINavigatioItem.h
//  stopstart
//
//  Created by cga on 09.03.19.
//  Copyright © 2019 PI-Data AG. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PIBarButtonItem : UIBarButtonItem
- (void)buttonPressed:(PIBarButtonItem *)button;
@end

NS_ASSUME_NONNULL_END
