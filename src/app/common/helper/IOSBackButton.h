//
//  IOSBackButton.h
//  stopstart
//
//  Created by cga on 20.02.19.
//  Copyright © 2019 PI-Data AG. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IOSBackButton : UIButton
- (void)buttonPressed:(IOSBackButton *)button;
@end

NS_ASSUME_NONNULL_END
