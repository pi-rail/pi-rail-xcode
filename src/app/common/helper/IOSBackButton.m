//
//  IOSBackButton.m
//  stopstart
//
//  Created by cga on 20.02.19.
//  Copyright © 2019 PI-Data AG. All rights reserved.
//
#import <JRE.h>
#import "IOSBackButton.h"
#import "MainViewController.h"
#import "DialogController.h"
#import "IOSDialog.h"
#import "Logger.h"

@implementation IOSBackButton


- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    [self addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  }
  return self;
}


- (void)buttonPressed:(IOSBackButton *)button {
  PICL_Logger_infoWithNSString_( @"IOSBackButton Pressed" );
  PIIC_UIViewControllerPI *controller = [self getViewController];
  [nil_chk([nil_chk([controller getIosDialog]) getController]) closeWithBoolean:true];
}

- (PIIC_UIViewControllerPI *)getViewController {
  UIView *parent = self.superview;
  while (parent != nil) {
    UIResponder *nextResponder = [parent nextResponder];
    if ([nextResponder isKindOfClass:[PIIC_UIViewControllerPI class]]) {
      return (PIIC_UIViewControllerPI *) nextResponder;
    }
    else {
      parent = parent.superview;
    }
  }
  return nil;
}

@end
