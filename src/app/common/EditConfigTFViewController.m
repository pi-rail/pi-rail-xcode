//
//  EditConfigTFViewController.m
//  PI-Rail-XCode
//
//  Created by cga on 04.10.21.
//  Copyright © 2021 PI-Data AG. All rights reserved.
//

#import "EditConfigTFViewController.h"
#import "IOSDialog.h"

@interface EditConfigTFViewController ()

@property (weak, nonatomic) IBOutlet UIView *module_1;
@property (weak, nonatomic) IBOutlet UIView *module_2;

@end

@class ComDaimlerPrototypeCommonUIPrototypeDialogDelegate;

@implementation EditConfigTFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional loadNib after loading the view.
}

+(void)setDlgDelegate:(ComDaimlerPrototypeCommonUIPrototypeDialogDelegate *)dlgDelegate{
    self.dlgDelegate = dlgDelegate;
}

- (IBAction)segmentSelected:(UISegmentedControl *)sender {
    [self showSelectedTab: sender.selectedSegmentIndex];
}

- (void)showSelectedTab:(NSInteger)tabIndex {
    NSLog(@"Tab selected: %ld", (long)tabIndex);
    
    if (tabIndex == 0) {
        self.module_1.hidden = false;
    }
    else {
        self.module_1.hidden = true;
    }
    
    if (tabIndex == 1) {
        self.module_2.hidden = false;
    }
    else {
        self.module_2.hidden = true;
    }
        
}

@end
